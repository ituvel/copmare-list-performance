﻿using ListPerf;

int limit = 500_000;
int iterations = 7;


Console.WriteLine($"list testing. {limit} items");



// классический список из фреймворка
Console.WriteLine("");
Console.WriteLine("System.Collections.Generic.List");

for (int i = 0; i < iterations; i++)
{
    RunTester(new List<HardValue>(), limit);
}

// первая реализация. объек оборачивается в сециальный класс, чтобы хранитьт ссылку на следующий
Console.WriteLine("");
Console.WriteLine("ListPerf.RealList_v0.RealList");

for (int i = 0; i < iterations; i++)
{
    RunTester(new ListPerf.RealList_v0.RealList<HardValue>(), limit);
}

// вторая реализация, чуть более общая (бесполезная в общем-то)
Console.WriteLine("");
Console.WriteLine("ListPerf.RealList_v1.RealListMoreGeneric");


for (int i = 0; i < iterations; i++)
{
    RunTester(new ListPerf.RealList_v1.RealListMoreGeneric<HardValue>(), limit);
}

// реализация без generic, объект должен самостоятель содержать ссылку на следующий элемент
Console.WriteLine("");
Console.WriteLine("ListPerf.RealList_v2.TrickyList");


for (int i = 0; i < iterations; i++)
{
    RunTester(new ListPerf.RealList_v2.TrickyList(), limit);
}

// классический список из фреймворка, ещё раз
Console.WriteLine("");
Console.WriteLine("System.Collections.Generic.List, again");

for (int i = 0; i < iterations; i++)
{
    RunTester(new List<HardValue>(), limit);
}



static void RunTester(ICollection<HardValue> list, int limit)
{

    Tester<HardValue> classicTester = new(limit, new HardValueGenerator());
    classicTester.Run(list, a => a.RandomValue, a => a.Id);

}

