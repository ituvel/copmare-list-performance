﻿namespace ListPerf
{
    public abstract class Generator<T>
    {
        public abstract T Create(int id);
    }

    public class HardValueGenerator : Generator<HardValue>
    {
        private static readonly Random random = new(DateTime.Now.Millisecond);

        public override HardValue Create(int id)
        {

            double d = 1000 * (random.NextDouble() - 0.5);


            return new HardValue
            {
                Id = id,
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.UtcNow,
                RandomValue = d,
                Name = $"HardValue {id} created {DateTime.Now} with double {d}"

            };
        }
    }

}
