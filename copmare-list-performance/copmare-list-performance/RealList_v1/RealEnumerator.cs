﻿using System.Collections;

namespace ListPerf.RealList_v1
{

    public class RealEnumerator<T> : IEnumerator<T>
    {
        private readonly IRealListItem<T> root;
        private IRealListItem<T> currentStoreItem;

        public RealEnumerator(IRealListItem<T> root)
        {
            this.root = root;
            currentStoreItem = root;
        }

        public T Current => currentStoreItem.Data;

        object IEnumerator.Current => currentStoreItem.Data;

        public void Dispose()
        {

        }

        public bool MoveNext()
        {
            currentStoreItem = currentStoreItem.Next;
            return currentStoreItem != null;
        }

        public void Reset()
        {
            currentStoreItem = root;
        }
    }
}
