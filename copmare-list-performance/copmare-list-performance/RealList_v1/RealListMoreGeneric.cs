﻿ 
namespace ListPerf.RealList_v1
{

    public class RealListItem<T> : IRealListItem<T>
    {
        public T? Data { get; set; }
        public IRealListItem<T>? Next { get; set; }
    }


    public class RealListMoreGeneric<T> : AbstractRealList<T>
    {
        public override void Add(T item)
        {
            IRealListItem<T> storeItem = new RealListItem<T>
            {
                Data = item
            };

            _root ??= storeItem;

            if (_tail == null)
            {
                _tail = storeItem;
            }
            else
            {
                _tail.Next = storeItem;
                _tail = storeItem;
            }


            Count++;
        }

    }
}
