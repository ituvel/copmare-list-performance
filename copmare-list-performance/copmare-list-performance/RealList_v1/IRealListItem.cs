﻿namespace ListPerf.RealList_v1
{
    public interface IRealListItem<T>
    {
        T? Data { get; set; }
        IRealListItem<T>? Next { get; set; }
    }
}
