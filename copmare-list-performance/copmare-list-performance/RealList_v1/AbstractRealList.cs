﻿using System.Collections;

namespace ListPerf.RealList_v1
{
    public abstract class AbstractRealList<T> : ICollection<T>
    {
        protected IRealListItem<T>? _root;
        protected IRealListItem<T>? _tail;

        public int Count { get; protected set; }

        public bool IsReadOnly => false;

        public abstract void Add(T item);

        public void Clear()
        {
            _root = null;
            _tail = null;
            Count = 0;
        }

        public bool Contains(T item)
        {
            throw new NotImplementedException();
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public IEnumerator<T> GetEnumerator()
        {
            return new RealEnumerator<T>(_root);
        }

        public bool Remove(T item)
        {
            throw new NotImplementedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }
}
