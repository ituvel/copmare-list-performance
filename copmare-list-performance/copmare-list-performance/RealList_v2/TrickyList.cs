﻿using System.Collections;

namespace ListPerf.RealList_v2
{

    public class TrickyList : ICollection<HardValue>
    {
        private HardValue? _root;
        private HardValue? _tail;

        public int Count { get; private set; }

        public bool IsReadOnly => false;

        public void Add(HardValue item)
        {
             

            _root ??= item;

            if (_tail == null)
            {
                _tail = item;
            }
            else
            {
                _tail.Next = item;
                _tail = item;
            }


            Count++;


        }

        public void Clear()
        {
            _root = null;
            _tail = null;
            Count = 0;
        }

        public bool Contains(HardValue item)
        {
            throw new NotImplementedException();
        }

        public void CopyTo(HardValue[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public IEnumerator<HardValue> GetEnumerator()
        {
            return new TrickyEnumerator(_root);
        }

        public bool Remove(HardValue item)
        {
            throw new NotImplementedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }
}
