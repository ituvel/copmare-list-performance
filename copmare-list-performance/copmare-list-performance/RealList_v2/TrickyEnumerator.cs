﻿using System.Collections;

namespace ListPerf.RealList_v2
{
    public class TrickyEnumerator : IEnumerator<HardValue>
    {
        private readonly HardValue root;
        private HardValue currentStoreItem;

        public TrickyEnumerator(HardValue root)
        {
            this.root = root;
            currentStoreItem = root;
        }

        public HardValue Current => currentStoreItem;

        object IEnumerator.Current => currentStoreItem ;

        public void Dispose()
        {

        }

        public bool MoveNext()
        {
            currentStoreItem = currentStoreItem.Next;
            return currentStoreItem != null;
        }

        public void Reset()
        {
            currentStoreItem = root;
        }
    }
}
