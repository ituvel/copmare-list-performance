﻿namespace ListPerf
{
    public class HardValue
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }

        public double RandomValue { get; set; }

        // закладываем указалель для списка с "лобовой" реализацикй
        public HardValue Next { get; set; }
    }

}
