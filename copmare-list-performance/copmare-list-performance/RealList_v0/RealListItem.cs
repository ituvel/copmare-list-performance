﻿namespace ListPerf.RealList_v0
{
    public class RealListItem<T>
    {
        public T? Data;
        public RealListItem<T>? Next;
    }
}
