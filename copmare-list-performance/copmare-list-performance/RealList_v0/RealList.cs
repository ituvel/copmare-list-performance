﻿using System.Collections;

namespace ListPerf.RealList_v0
{

    public class RealList<T> : ICollection<T>
    {
        private RealListItem<T>? _root;
        private RealListItem<T>? _tail;

        public int Count { get; private set; }

        public bool IsReadOnly => false;

        public void Add(T item)
        {
            RealListItem<T> storeItem = new()
            {
                Data = item
            };

            _root ??= storeItem;

            if (_tail == null)
            {
                _tail = storeItem;
            }
            else
            {
                _tail.Next = storeItem;
                _tail = storeItem;
            }


            Count++;


        }

        public void Clear()
        {
            _root = null;
            _tail = null;
            Count = 0;
        }

        public bool Contains(T item)
        {
            throw new NotImplementedException();
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public IEnumerator<T> GetEnumerator()
        {
            return new RealEnumerator<T>(_root);
        }

        public bool Remove(T item)
        {
            throw new NotImplementedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }
}
