﻿using System.Collections;

namespace ListPerf.RealList_v0
{
    public class RealEnumerator<T> : IEnumerator<T>
    {
        private readonly RealListItem<T> root;
        private RealListItem<T> currentStoreItem;

        public RealEnumerator(RealListItem<T> root)
        {
            this.root = root;
            currentStoreItem = root;
        }

        public T Current => currentStoreItem.Data;

        object IEnumerator.Current => currentStoreItem.Data;

        public void Dispose()
        {

        }

        public bool MoveNext()
        {
            currentStoreItem = currentStoreItem.Next;
            return currentStoreItem != null;
        }

        public void Reset()
        {
            currentStoreItem = root;
        }
    }
}
