﻿using System.Diagnostics;

namespace ListPerf
{
    public class Tester<T>
    {
        private readonly int limit;
        private readonly Generator<T> generator;

        public double summ { get; private set; }

        public Tester(int limit, Generator<T> generator)
        {
            this.limit = limit;
            this.generator = generator;
        }

        public void Run(ICollection<T> values, Func<T, double> valueAccessor, Func<T, int> idAccessor)
        {
            Process process = Process.GetCurrentProcess();

            long gc1 = GC.GetTotalMemory(true);

            long timeToAdd = AddItems(values);

            long timeToSumm = Summ(values, valueAccessor);

            long gc3 = GC.GetTotalMemory(true);

            Console.WriteLine($"{values.GetType().Name}\t" +
                $"limit: {limit},\t" +
                $"add time {timeToAdd} ms,\t" +
                $"summ time {timeToSumm} ms,\t" +
                $"memory {gc3 - gc1} bytes,\t" +
                $"{values.Sum(a => (decimal)idAccessor(a))}");

        }

        private long Summ(ICollection<T> values, Func<T, double> accessor)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();

            summ = 0;
            foreach (T? item in values)
            {
                summ += accessor(item);
            }


            stopwatch.Stop();

            return stopwatch.ElapsedMilliseconds;
        }

        private long AddItems(ICollection<T> values)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();

            for (int i = 0; i < limit; i++)
            {
                values.Add(generator.Create(i));

            }

            stopwatch.Stop();

            return stopwatch.ElapsedMilliseconds;
        }
    }
}
